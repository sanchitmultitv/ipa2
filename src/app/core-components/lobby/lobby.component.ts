import { Component, OnInit, ViewChild, ElementRef, OnDestroy, AfterViewInit, HostListener, Renderer2 } from '@angular/core';
import { Router } from '@angular/router';
declare var introJs: any;
declare var $: any;
import * as Clappr from 'clappr';
import { FetchDataService } from 'src/app/services/fetch-data.service';
import { ChatService } from 'src/app/services/chat.service';

@Component({
  selector: 'app-lobby',
  templateUrl: './lobby.component.html',
  styleUrls: ['./lobby.component.scss']
})
export class LobbyComponent implements OnInit, OnDestroy, AfterViewInit {
  
  videoEnd = false;
  receptionEnd = false;
  showVideo = false;
  auditoriumLeft = false;
  auditoriumRight = false;
  exhibitionHall = false;
  registrationDesk = false;
  networkingLounge = false;
  intro: any;
  player: any;
  actives: any = [];
  liveMsg = false;
  showPoster = false;
  cameraStream;
  showImage = false;
  videosource="assets/IPA/Mission_low.mp4"
  // videoUrl;
  @ViewChild('recepVideo', { static: true }) recepVideo: ElementRef;
  @ViewChild('videoStream', { static: true }) videoElement: ElementRef;
  constructor(private renderer: Renderer2,private router: Router, private _fd: FetchDataService, private chat: ChatService) { }
  // videoPlayer = 'https://s3.ap-south-1.amazonaws.com/acma.multitvsolution.in/assets/acme/acma-home_1.mp4';
  ngOnInit(): void {
    if (localStorage.getItem('user_guide') === 'start') {
      this.getUserguide();
    }
    this.stepUpAnalytics('click_lobby');
    this.audiActive();
    this.chat.getconnect('toujeo-60');
    this.chat.getMessages().subscribe((data => {
      //  console.log('data',data);
      if (data == 'start_live') {
        this.liveMsg = true;
      }
      if (data == 'stop_live') {
        this.liveMsg = false;
      }

    }));

    let playVideo: any = document.getElementById("playVideo");
    window.onclick = (event) => {
      if (event.target == playVideo) {
        playVideo.style.display = "none";
        let pauseVideo: any = document.getElementById("video");
        pauseVideo.currentTime = 0;
        pauseVideo.pause();
      }
    }
  }

  endPlayscreen(){
    this.showPoster = true;
  }
  playScreenOnImg(){
    this.showPoster = false;
    let playVideo: any = document.getElementById("playVideo");
    playVideo.play();  
  }
  ngAfterViewInit() {
    this.playAudio();
  }
 /*  openModalVideo() {
    $('#playVideo').modal('show');
    let vid: any = document.getElementById("video");
    vid.play();
  } */
  closeModalVideo(videoId = "video", modalId = "playVideo") {
    let pauseVideo: any = document.getElementById(videoId);
    pauseVideo.currentTime = 0;
    pauseVideo.pause();
     $('#' + modalId).modal('hide');
   /*  $('#playVideo1').modal('hide'); */
  }
  closePopup() {
    $('.yvideo').modal('hide');
  }
  openpopup(){
    if (localStorage.getItem('whatever')) {
      $('.NoFeedback').modal('show');
    }else{
      $('.feebackModal').modal('show');
    }
  }
  
  closePopuptwo() {
    $('.NoFeedback').modal('hide');
  }

  audiActive() {
    this._fd.activeAudi().subscribe(res => {
      this.actives = res.result;
    })
  }

  @HostListener('window:resize', ['$event']) onResize(event) {
    if (window.innerWidth <= 767) {
      this.player.resize({
        width: window.innerWidth / 6.69, height: window.innerHeight / 7.95
      });

    } else {
      this.player.resize({
        width: window.innerWidth / 6.69, height: window.innerHeight / 7.95
      });
    }
  }
  playAudio() {
    localStorage.setItem('play', 'play');
    let abc: any = document.getElementById('myAudio');
    abc.play();
    // alert('after login audio')
  }
  playevent() {
    this.videoEnd = true;
  }
  playReception() {
    this.receptionEnd = true;
    this.showVideo = true;
    let vid: any = document.getElementById("recepVideo");
    vid.play();
    let pauseVideo: any = document.getElementById("video");
    pauseVideo.currentTime = 0;
    pauseVideo.pause();
  }
  receptionEndVideo() {
    this.router.navigate(['/welcome']);

  }
  gotoKeyPortfolio() {
    this.registrationDesk = true;
    this.showVideo = true;
    let vid: any = document.getElementById("regDeskvideo");
    vid.play();
    let pauseVideo: any = document.getElementById("video");
    pauseVideo.currentTime = 0;
    pauseVideo.pause();
  }
  onEndKeyPortfolio() {
    this.router.navigate(['/keyportfolio']);

  }

  // gotoAuditoriumFront() {
  //   this.router.navigate(['/auditorium/front-desk']);
  // }

  playAuditoriumLeft() {
    // if (this.actives[3].status == true) {
      this.auditoriumLeft = true;
      this.showVideo = true;
      let vid: any = document.getElementById("audLeftvideo");
      vid.play();
      // let pauseVideo: any = document.getElementById("video");
      // pauseVideo.currentTime = 0;
      // pauseVideo.pause();
    }
    // else {
    //   $('.audiModal').modal('show');
    // }

  // }
  gotoAuditoriumLeftOnVideoEnd() {
    this.router.navigate(['/auditorium/one']);
  }
  playAuditoriumRight() {
    // if (this.actives[2].status == true) {
      this.auditoriumRight = true;
      this.showVideo = true;
      let vid: any = document.getElementById("audRightvideo");
      vid.play();
      // let pauseVideo: any = document.getElementById("video");
      // pauseVideo.currentTime = 0;
      // pauseVideo.pause();
    // }
    // else {
    //   $('.audiModal').modal('show');
    // }
  }
  gotoAuditoriumRightOnVideoEnd() {
    this.router.navigate(['/auditorium/one']);
  }
  playExhibitionHall() {
    this.exhibitionHall = true;
    this.showVideo = true;
    let vid: any = document.getElementById("exhibitvideo");
    vid.play();
    let pauseVideo: any = document.getElementById("video");
    pauseVideo.currentTime = 0;
    pauseVideo.pause();

  }
  gotoExhibitionHallOnVideoEnd() {
    this.router.navigate(['/exhibitionHall']);
  }
  // playRegistrationDesk() {
  //   this.registrationDesk = true;
  //   this.showVideo = true;
  //   let vid: any = document.getElementById("regDeskvideo");
  //   vid.play();
  // }
  // gotoRegistrationDeskOnVideoEnd() {
  //   this.router.navigate(['/auditorium/front-desk']);
  // }
  playNetworkingLounge() {
    this.networkingLounge = true;
    this.showVideo = true;
    let vid: any = document.getElementById("netLoungevideo");
    vid.play();
    let pauseVideo: any = document.getElementById("video");
    pauseVideo.currentTime = 0;
    pauseVideo.pause();

  }
  gotoNetworkingLoungeOnVideoEnd() {
    this.router.navigate(['/capturePhoto']);
  }
  lightbox_open() {
    //this.videoUrl = video;
    let lightBoxVideo: any = document.getElementById('VisaChipCardVideo');
    window.scrollTo(0, 0);
    document.getElementById('light').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
    lightBoxVideo.play();

  }
  lightbox_close() {
    let lightBoxVideo: any = document.getElementById('VisaChipCardVideo');
    document.getElementById('light').style.display = 'none';
    document.getElementById('fade').style.display = 'none';
    lightBoxVideo.pause();
  }
  stepUpAnalytics(action) {
    let virtual: any = JSON.parse(localStorage.getItem('virtual'));
    let yyyy: any = new Date().getFullYear();
    let dd: any = new Date().getDate();
    let mm: any = new Date().getMonth() + 1;
    let time: any = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    const formData = new FormData();
    formData.append('event_id', '183');
    formData.append('user_id', virtual.id);
    formData.append('name', virtual.name);
    formData.append('email', virtual.email);
    formData.append('company', 'others');
    formData.append('designation', 'others');
    formData.append('action', action);
    formData.append('created', yyyy + '-' + mm + '-' + dd + ' ' + time);
    this._fd.analyticsPost(formData).subscribe(res => {
      console.log('asdf', res);
    });
  }
  getUserguide() {
    this.intro = introJs().setOptions({
      hidePrev: true,
      hideNext: true,
      exitOnOverlayClick: false,
      exitOnEsc: false,
      steps: [
        {
          element: document.querySelectorAll("#audLeft_pulse")[0],
          intro: "<div style='text-align:center'>All the LIVE sessions will be running in the auditorium on schedule</div>"
        },
        {
          element: document.querySelectorAll("#agenda_pulse")[0],
          intro: "<div style='text-align:center'>Click here to see the speaker profile</div>"
        },
        {
          element: document.querySelector("#frontaudi_pulse"),
           intro: "<div style='text-align:center'>Click here to see the Gratitude Wall</div>"
         },
        {
          element: document.querySelector("#reception_pulse"),
          intro: "<div style='text-align:center'>For any queries Relating to the event please visit at the Help desk</div>"
        },
        {
          element: document.querySelectorAll("#heighlight_pulse")[0],
          intro: "<div style='text-align:center'>The agenda for the event can be viewed in agenda section</div>"
        },
        {
          element: document.querySelectorAll("#networking_pulse")[0],
          intro: "<div style='text-align:center'>Click a selfie, visit the Photobooth</div>"
        },
       /*  {
          element: document.querySelectorAll("#exhibition_pulse")[0],
          intro: "<div style='text-align:center'>Do visit the stalls placed in the Exhibition area.</div>"
        }, */
        
       /*  {
          element: document.querySelectorAll("#audRight_pulse")[0],
          intro: "<div style='text-align:center'>All the LIVE sessions will be running in the auditorium on schedule.</div>"
        }, */
        
       
        


      ]
    }).oncomplete(() => document.cookie = "intro-complete=true");

    let start = () => this.intro.start();
    start();
    // if (document.cookie.split(";").indexOf("intro-complete=true") < 0)
    //   window.setTimeout(start, 1000);
  }
  ngOnDestroy() {
    if (localStorage.getItem('user_guide') === 'start') {
      let stop = () => this.intro.exit();
      stop();
    }
    localStorage.removeItem('user_guide');
  }
 
  /* photobooth ts bottom */
 
  showPhotobooth(){
    $('#photoboothModal').modal('show');
    this.cameraStream = null;
    this.showImage = false;
    this.startWebcam();
  }
  startWebcam(){
    // const constraints = { "video": { width: 320, height: 180, facingMode: "user" }};
    let constraints = {
      facingMode: { exact: 'environment' },
      video: {
        width: { ideal: 640 },
        height: { ideal: 360 }
      }
    };
    navigator.mediaDevices.getUserMedia(constraints).then(this.gotMedia.bind(this)).catch(e=>{
        console.error('getusermedia() failed: '+e);
    })
  }
  theRecorder:any;
  gotMedia(stream){
    this.cameraStream = stream;
    this.renderer.setProperty(this.videoElement.nativeElement, 'srcObject', stream);
  }
  dataUrl;
  img;
  capturePhoto(){
    this.showImage = true;
    let canvasRec:any = document.getElementById('canvas');
    let preview:any = document.getElementById('videobgStream');
    let context = canvasRec.getContext('2d');
    var cw = 640;
    var ch = 360;
    canvasRec.width = cw;
    canvasRec.height = ch;
    context.drawImage(preview, 0, 0, cw, ch ); 
       
    let watermark = new Image();
    let bgimg = new Image();
    context.beginPath();
    
    bgimg.src = 'assets/IPA/photo jacket (1).png';
    context.drawImage(bgimg, 0, 0, preview.videoWidth, preview.videoHeight);
    this.dataUrl = canvasRec.toDataURL('mime');
    this.img = canvasRec.toDataURL("image/png", 0.7);
  }
  @HostListener('document:click', ['$event', '$event.target'])
    onClick(event: MouseEvent, targetElement: HTMLElement): void {
      let selfie:any=document.getElementById('photoboothModal');
      if(targetElement===selfie){
        this.closePhotobooth();
      }
  }
  @HostListener('keydown', ['$event']) onKeyDown(e) {
    if (e.keyCode == 27) {
      this.closePhotobooth();
    }
  }
  closePhotobooth(){
    this.cameraStream.getTracks().forEach(track => { track.stop(); }); 
    $('#photoboothModal').modal('hide');
    // this.location.back();
  }
  downloadPic(){
    window.location.href=this.img;
    let user_id = JSON.parse(localStorage.getItem('virtual')).id;
    let user_name = JSON.parse(localStorage.getItem('virtual')).name;
    const event_id = '178';
    const formData = new FormData();
    formData.append('user_id', user_id);
    formData.append('user_name', user_name);
    formData.append('image', this.img);
    formData.append('event_id', event_id);
    this._fd.uploadsample(formData).subscribe(res => {
      console.log('upload', res);
    });
    
  }
  reload() {
    this.cameraStream.getTracks().forEach(track => { track.stop(); }); 
    this.showImage = false;
    this.startWebcam();
  }

  closeModalVideo1(){
    $("#closePopup").click(function() {
      $('#video').pause();
    });
    $('#playVideo1').modal('hide')
  }
  
}
