import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { KeyPortfolioComponent } from './key-portfolio/key-portfolio.component';


const routes: Routes = [
  {path:'', component:KeyPortfolioComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class KeyPortfolioRoutingModule { }
