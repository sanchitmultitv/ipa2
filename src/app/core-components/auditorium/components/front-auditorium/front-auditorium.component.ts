import { Component, OnInit } from '@angular/core';
import { GroupChatThreeComponent } from '../../../../layout/group-chat-three/group-chat-three.component';
declare var $:any;
@Component({
  selector: 'app-front-auditorium',
  templateUrl: './front-auditorium.component.html',
  styleUrls: ['./front-auditorium.component.scss'],
  providers:[GroupChatThreeComponent]
})
export class FrontAuditoriumComponent implements OnInit {

  videoEnd = false;
  videoPlayerThree = 'https://dhzjgfv9krlhb.cloudfront.net/abr/smil:stream2.smil/playlist.m3u8';
  
  constructor(private gc:GroupChatThreeComponent) { }
  ngOnInit(): void {
    console.log(this.videoPlayerThree,'this is 3');
  }
  
  videoEnded() {
    this.videoEnd = true;
  }
  playAudioClap(){
    let playaudio : any= document.getElementById('myAudioClap');
    playaudio.play();
  }
  playAudioWhistle(){
    let playaudio : any= document.getElementById('myAudioWhistle');
    playaudio.play();
  }
  openGroupChat(){
    $('.groupchatsModalThree').modal('show')
    this.gc.loadData();
  }
}
