import { Component, OnInit, OnDestroy } from '@angular/core';
import { fadeAnimation } from '../../../../shared/animation/fade.animation';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { FetchDataService } from '../../../../services/fetch-data.service'
import { EventEmitter } from 'events';
import { ChatService } from '../../../../services/chat.service';
declare var $: any;

@Component({
  selector: 'app-auditorium-one',
  templateUrl: './auditorium-one.component.html',
  styleUrls: ['../auditorimStl/audi.style.scss'],
  animations: [fadeAnimation],

})
export class AuditoriumOneComponent implements OnInit, OnDestroy {
  videoEnd = false;
  videoPlayer = 'https://d17uqpjc0q0ra5.cloudfront.net/abr/smil:session4.smil/playlist.m3u8';
  posterIMG="assets/IPA/Stageviewwithtab.jpg"
  interval;
  like = true;
  imag:any;
  actives: any;
  activeAuditoriums: any;
  activeStream: any;
  // videoPlayer = '../assets/video/networking_lounge_video.mp4';
  constructor(private chatService: ChatService, private _fd: FetchDataService) { }

  ngOnInit(): void 
  {

     this.audiActive();
    this.interval= setInterval(() => {
      this.getHeartbeat(); 
         }, 60000);
    console.log(this.videoPlayer, 'this is 1');
    this.loadData();
    this.chatService.getconnect('toujeo-183');
    this.chatService.getMessages().subscribe((data => {
      //  console.log('data',data);
      if (data == 'groupchat') {
        this.chatGroup();
      }

    }));
  }
  audiActive() {
    this._fd.activeAudi().subscribe(res => {
      console.log(res, 'resssss');
      this.actives = res.result;
      this.activeAuditoriums = this.actives[0].poster;
      this.activeStream = this.actives[0].stream;
      //alert(this.actives[0].poster);
    })
  }
  videoEnded() {
    this.videoEnd = true;
  }
  getHeartbeat(){
    let data = JSON.parse(localStorage.getItem('virtual'));
    const formData = new FormData();
    formData.append('user_id', data.id );
    formData.append('event_id', '183');
    formData.append('audi', '75');
    this._fd.heartbeat(formData).subscribe(res=>{
      console.log(res);
    })
  }
  
  playAudioClap() {
    let playaudio: any = document.getElementById('myAudioClap');
    playaudio.play();
  }
  playAudioWhistle() {
    let playaudio: any = document.getElementById('myAudioWhistle');
    playaudio.play();
  }
  openGroupChat() {
     $('.groupchatOne').modal('show');
    this.messageList = [];
    this.loadData(); 
  }


  textMessage = new FormControl('');
  newMessage: string[] = [];
  msgs: string;
  messageList: any = [];
  roomName = 'IPA';
  serdia_room = localStorage.getItem('serdia_room');
  loadData() {
    this.chatGroup();
    this.chatService.getconnect('toujeo-183');
    let data = JSON.parse(localStorage.getItem('virtual'));
    this.chatService.addUser(data.name, this.roomName);
    localStorage.setItem('username', data.name);
    this.chatService.receiveMessages(this.roomName).subscribe((msgs: any) => {
      if (msgs.roomId === 1) {
        this.messageList.push(msgs);
      }
      console.log('demo', this.messageList);
    });
  }
  chatGroup() {
    this._fd.groupchating().subscribe(res => {
      console.log('groupChat', res);
      this.messageList = res.result;
    });
  }

  closePopup() {
    $('.groupchatOne').modal('hide');
  }
  postMessage(value) {
    let data = JSON.parse(localStorage.getItem('virtual'));
    let yyyy: any = new Date().getFullYear();
    let dd: any = new Date().getDate();
    let mm: any = new Date().getMonth() + 1;
    let time: any = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    let created = yyyy + '-' + mm + '-' + dd + ' ' + time;
  //  this.chatService.sendMessage(value, data.name, this.roomName);
  this._fd.postGroupchat(value,data.name,data.email, this.roomName, created,data.id).subscribe(res=>{
    console.log(res);
  })

    this.textMessage.reset();
    // this.chatGroup();
    //this.newMessage.push(this.msgs);
  }
  likeopen(data){
    if(data =='like'){
      this.imag = 'assets/honeywell/like.png'
    }
    if(data =='claps'){
      this.imag = 'assets/honeywell/claps.png'
    }
    if(data =='heart'){
      this.imag = 'assets/honeywell/heart.png'
    }
    this.like = true;
    setTimeout(() => {
      this.like = false;
    }, 10000);
  }
  ngOnDestroy() {
    clearInterval(this.interval);
    // this.chatService.disconnect();
  }
}

