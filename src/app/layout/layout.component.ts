
  import { Component, OnInit, Renderer2, ViewChild, ElementRef, Output, HostListener, AfterViewInit } from '@angular/core';
  import { fadeAnimation } from '../shared/animation/fade.animation';
  import { Router } from '@angular/router';
  import { EventEmitter } from 'events';
  import { AuthService } from '../services/auth.service';
  import { FetchDataService } from '../services/fetch-data.service';
  import { Location } from '@angular/common';
  declare var $:any;
  @Component({
    selector: 'app-layout',
    templateUrl: './layout.component.html',
    styleUrls: ['./layout.component.scss'],
    animations: [fadeAnimation]
  
  })
  export class LayoutComponent implements OnInit, AfterViewInit {
    firstName;
    showImage = false;
    landscape = true;
    actives: any = [];
    activeAuditoriums = [];
    cameraStream;
    
   
    img;  
    dataUrl: any;
    constructor( private location: Location,public router: Router, private renderer: Renderer2, private auth: AuthService, private _fd: FetchDataService) { }
  
    ngOnInit(): void {
      this.audiActive();
      $('.videoData').show();
      this.firstName = JSON.parse(localStorage.getItem('virtual')).name;
      this.playAudio();
    }
    ngAfterViewInit() {
      if (window.innerHeight > window.innerWidth) {
        this.landscape = false;
      }
    }
    public getRouterOutletState(outlet) {
      return outlet.isActivated ? outlet.activatedRoute : '';
    }
    showModal = false;
    toggleModal() {
      this.showModal = !this.showModal;
    }
  
    readOutputValueEmitted(event) {
      console.log('kkkk', event)
    }
  
    videoWidth = 0;
    videoHeight = 0;
    @ViewChild('videoStream', { static: true }) videoElement: ElementRef;
    /*  @ViewChild('video', { static: true }) videoElement: ElementRef; */ 
    @ViewChild('canvas', { static: true }) canvas: ElementRef; 
    @Output('myOutputVal') myOutputVal = new EventEmitter();
    playAudio() {
      let abc: any = document.getElementById('myAudio');
      abc.play();
    }
    constraints = {
      video: {
        facingMode: "environment",
        width: { ideal: 720 },
        height: { ideal: 480 }
      }
    };
    audis = ['Audi 1', 'Audi 2'];
    audiActive() {
      this._fd.activeAudi().subscribe(res => {
        console.log(res, 'resssss');
        this.actives = res.result;
        this.activeAuditoriums = res.result;
        console.log(this.actives[0]);
      })
    }
    gotoAudi(audi) {
      if (audi === 1) {
        localStorage.setItem('serdia_room', 'myanmar_16');
        if (this.actives[3].status == true) {
          this.router.navigate(['/auditorium/one']);
        }
        else {
          $('.audiModal').modal('show');
        }
      }
      if (audi === 2) {
        localStorage.setItem('serdia_room', 'myanmar_17');
        if (this.actives[2].status == true) {
          this.router.navigate(['/auditorium/two']);
        }
        else {
          $('.audiModal').modal('show');
        }
      }
      if (audi === 3) {
        localStorage.setItem('serdia_room', 'myanmar_18');
        if (this.actives[1].status == true) {
          this.router.navigate(['/auditorium/three']);
        }
        else {
          $('.audiModal').modal('show');
  
        }
      }
      if (audi === 4) {
        localStorage.setItem('serdia_room', 'myanmar_19');
        // this.router.navigate(['/auditorium/four']);
        if (this.actives[0].status == true) {
          this.router.navigate(['/auditorium/fourth-auditorium']);
        }
        else {
          $('.audiModal').modal('show');
        }
  
      }
    }
  
    startCamera() {
      if (!!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia)) {
        navigator.mediaDevices.getUserMedia(this.constraints).then(this.attachVideo.bind(this)).catch(this.handleError);
      } else {
        alert('Sorry, camera not available.');
      }
  
    }
    handleError(error) {
      console.log('Error: ', error);
    }
    stepUpAnalytics(action) {
      let virtual: any = JSON.parse(localStorage.getItem('virtual'));
      let yyyy: any = new Date().getFullYear();
      let dd: any = new Date().getDate();
      let mm: any = new Date().getMonth() + 1;
      let time: any = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      const formData = new FormData();
      formData.append('event_id', '183');
      formData.append('user_id', virtual.id);
      formData.append('name', virtual.name);
      formData.append('email', virtual.email);
      formData.append('company', 'others');
      formData.append('designation', 'others');
      formData.append('action', action);
      formData.append('created', yyyy + '-' + mm + '-' + dd + ' ' + time);
      this._fd.analyticsPost(formData).subscribe(res => {
        console.log('asdf', res);
      });
    }
  
    stream: any;
    attachVideo(stream) {
      this.renderer.setProperty(this.videoElement.nativeElement, 'srcObject', stream);
      this.renderer.listen(this.videoElement.nativeElement, 'play', (event) => {
        this.videoHeight = this.videoElement.nativeElement.videoHeight;
        this.videoWidth = this.videoElement.nativeElement.videoWidth;
      });
    }
    
    capture() {
      this.showImage = true;
      this.renderer.setProperty(this.canvas.nativeElement, 'width', this.videoWidth);
      this.renderer.setProperty(this.canvas.nativeElement, 'height', this.videoHeight);
      this.canvas.nativeElement.getContext('2d').drawImage(this.videoElement.nativeElement, 0, 0);
      let canvas: any = document.getElementById("canvas");
      let context = canvas.getContext('2d');
      context.strokeStyle = "#ffc90e";
      context.lineWidth = 5;
      var img = document.getElementById("logo");
      context.drawImage(img, 5, 5, 120, 50);
      context.strokeRect(0, 0, canvas.width, canvas.height);
      let imgPath = '../../../assets/img/xtrem.jpeg';
      let imgObj = new Image();
  
      imgObj.src = imgPath;
      this.img = canvas.toDataURL("image/png");
      console.log('kkkk', imgObj.src) 
      // $('.videoData').hide();
    }
    stopVideo() {
      this.renderer.listen(this.videoElement.nativeElement, 'stop', (event) => {
        console.log('dsfj')
      });
    }
  
     showCapture() {
      $('.capturePhoto').modal('show');
      this.startCamera();
    }
  
    closeCamera() {
      location.reload();
      $('.capturePhoto').modal('hide');
  
    } 
    closePopup() {
      $('.feebackModal1').modal('hide');
    }
    playAudioClap() {
      let playaudio: any = document.getElementById('myAudioClap');
      playaudio.play();
    }
  
    openSidebar() {
      document.getElementById("mySidenav").style.width = "220px";
    }
  
    closeSidebar() {
      document.getElementById("mySidenav").style.width = "0";
    }
    @HostListener('window:resize', ['$event']) onResize(event) {
      if (window.innerHeight > window.innerWidth) {
        this.landscape = false;
      } else {
        this.landscape = true;
      }
    }
    logout() {
     
      let user_id = JSON.parse(localStorage.getItem('virtual')).id;
      this.auth.logout(user_id).subscribe(res => {
        this.router.navigate(['/login']);
        localStorage.clear();      
      });
    }
  
   
    showPhotobooth(){
      $('#photoboothModal').modal('show');
      this.cameraStream = null;
      this.showImage = false;
      this.startWebcam();
    }
    closePhotobooth(){
      this.cameraStream.getTracks().forEach(track => { track.stop(); }); 
      $('#photoboothModal').modal('hide');
      // this.location.back();
    }
    downloadPic(){
      window.location.href=this.img;
      let user_id = JSON.parse(localStorage.getItem('virtual')).id;
      let user_name = JSON.parse(localStorage.getItem('virtual')).name;
      const event_id = '178';
      const formData = new FormData();
      formData.append('user_id', user_id);
      formData.append('user_name', user_name);
      formData.append('image', this.img);
      formData.append('event_id', event_id);
      this._fd.uploadsample(formData).subscribe(res => {
        console.log('upload', res);
      });
      
    }
    reload() {
      this.cameraStream.getTracks().forEach(track => { track.stop(); }); 
      this.showImage = false;
      this.startWebcam();
    }
    
    startWebcam(){
      // const constraints = { "video": { width: 320, height: 180, facingMode: "user" }};
      let constraints = {
        facingMode: { exact: 'environment' },
        video: {
          width: { ideal: 640 },
          height: { ideal: 360 }
        }
      };
      navigator.mediaDevices.getUserMedia(constraints).then(this.gotMedia.bind(this)).catch(e=>{
          console.error('getusermedia() failed: '+e);
      })
    }
    theRecorder:any;
  gotMedia(stream){
    this.cameraStream = stream;
    this.renderer.setProperty(this.videoElement.nativeElement, 'srcObject', stream);
  }
  capturePhoto(){
    this.showImage = true;
    let canvasRec:any = document.getElementById('canvas');
    let preview:any = document.getElementById('videobgStream');
    let context = canvasRec.getContext('2d');
    var cw = 640;
    var ch = 360;
    canvasRec.width = cw;
    canvasRec.height = ch;
    context.drawImage(preview, 0, 0, cw, ch ); 
       
    let watermark = new Image();
    let bgimg = new Image();
    context.beginPath();
    
    bgimg.src = 'assets/IPA/photo jacket (1).png';
    context.drawImage(bgimg, 0, 0, preview.videoWidth, preview.videoHeight);
    this.dataUrl = canvasRec.toDataURL('mime');
    this.img = canvasRec.toDataURL("image/png", 0.7);
  }   
   
  
  }